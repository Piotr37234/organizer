package com.example.uytkownik.organizerczasu;

import android.app.usage.UsageEvents;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Kalendarz extends AppCompatActivity {


    public ZadaniaSQLITE em;
    int wybraneId;
    private static final String DEBUG_TAG = "ErrorKalendarz:";
    TableLayout ukladTabeli;
    CalendarView calendarView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalendarz);
        ukladTabeli = findViewById(R.id.TabelaPrzyKalendarzu);

        em = new ZadaniaSQLITE(this);
        em.open();
        calendarView = findViewById(R.id.Kalendarz);

        usunWierszeTabeli();

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Dane.setDomyslna_data(year+"."+month+"."+dayOfMonth);
                usunWierszeTabeli();
                aktualizujWierszeTabeli();

            }
        });

        aktualizujWierszeTabeli();
    }


    public void dodajWiersz(TableLayout tabela, Zadanie zadanie) {
        TableRow row = new TableRow(this);
        row.setBackground(getDrawable(R.drawable.border_style));
        row.setId(zadanie.getId());
        row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                klikZaznacz(v);
            }
        });
        TextView u1 = new TextView(this);
        u1.setText(zadanie.getNazwaZadania());
        u1.setTextSize(14);
        u1.setLayoutParams(new TableRow.LayoutParams(128, TableRow.LayoutParams.MATCH_PARENT));
        u1.setTextColor(Color.BLACK);
        u1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        row.addView(u1);
        tabela.addView(row);
    }

    public void dodajWierszNaglowka(TableLayout tabela) {
        TableRow row = new TableRow(this);
        row.setBackground(getDrawable(R.drawable.border_style));
        row.setId(0);
        TextView u1 = new TextView(this);
        u1.setText("Zadania");
        u1.setTextSize(16);
        u1.setLayoutParams(new TableRow.LayoutParams(210, TableRow.LayoutParams.MATCH_PARENT));
        u1.setTextColor(Color.RED);
        u1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        row.addView(u1);
        tabela.addView(row);
    }



    public void klikZaznacz(View view) {
        if (wybraneId != 0) {
            findViewById(wybraneId).setBackground(getDrawable(R.drawable.border_style));
        }
        view.setBackgroundColor(Color.argb(21, 169, 211, 231));
        view.setBackground(getDrawable(R.drawable.selected_style));
        wybraneId = view.getId();
        Log.d(DEBUG_TAG, "Wybrane ID=" + wybraneId);
    }

public void usunWierszeTabeli(){
    ukladTabeli.removeAllViews();
}


    public void aktualizujWierszeTabeli() {
        dodajWierszNaglowka(ukladTabeli);
        for (Zadanie zadanie : em.getListaZadanZdaty()) {
            dodajWiersz(ukladTabeli, zadanie);
        }
    }

    public void klikDodaj(View view) {
       // Dane.setDomyslna_data(Data);
        //Log.d(DEBUG_TAG, "Wybrane ID=" + wybraneID);
        finish();
        Intent intencja = new Intent(Kalendarz.this, Dodaj.class);

        startActivity(intencja);
    }


    public void klikUsun(View view) {
        if (wybraneId == 0) {
            return;
        }
        em.usunRekordZTabeliZadanie(wybraneId);
        recreate();
        wybraneId = 0;
    }

    public void onBackPressed() {
        finish();
        Intent intencja = new Intent(Kalendarz.this, MenuOrganizera.class);
        startActivity(intencja);
    }


}
