package com.example.uytkownik.organizerczasu;


public class Zadanie {
    int id;
    String nazwaZadania;
    String terminWykonania;
    int iloscPotrzebnychDni;
    int waznosc;
    int pilnosc;
    int id_uzytkownika;


    public Zadanie(int id, String nazwaZadania, String terminWykonania, int iloscPotrzebnychDni, int waznosc, int pilnosc, int id_uzytkownika) {
        this.id = id;
        this.nazwaZadania = nazwaZadania;
        this.terminWykonania = terminWykonania;
        this.iloscPotrzebnychDni = iloscPotrzebnychDni;
        this.waznosc = waznosc;
        this.pilnosc = pilnosc;
        this.id_uzytkownika = id_uzytkownika;
    }





    public int getId_uzytkownika() {
        return id_uzytkownika;
    }

    public void setId_uzytkownika(int id_uzytkownika) {
        this.id_uzytkownika = id_uzytkownika;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwaZadania() {
        return nazwaZadania;
    }

    public void setNazwaZadania(String nazwaZadania) {
        this.nazwaZadania = nazwaZadania;
    }

    public String getTerminWykonania() {
        return terminWykonania;
    }

    public void setTerminWykonania(String terminWykonania) {
        this.terminWykonania = terminWykonania;
    }

    public int getIloscPotrzebnychDni() {
        return iloscPotrzebnychDni;
    }

    public void setIloscPotrzebnychDni(int iloscPotrzebnychDni) {
        this.iloscPotrzebnychDni = iloscPotrzebnychDni;
    }

    public int getWaznosc() {
        return waznosc;
    }

    public void setWaznosc(int waznosc) {
        this.waznosc = waznosc;
    }

    public int getPilnosc() {
        return pilnosc;
    }

    public void setPilnosc(int pilnosc) {
        this.pilnosc = pilnosc;
    }




}
