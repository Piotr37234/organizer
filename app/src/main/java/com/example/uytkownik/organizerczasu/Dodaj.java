package com.example.uytkownik.organizerczasu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dodaj extends AppCompatActivity {

    EditText poleNazwaZadania;



    EditText poleTermin;
    EditText poleDlugoscWDniach;
    SeekBar suwakPilnoscZadania;
    SeekBar suwakWaznoscZadania;


    public ZadaniaSQLITE em;
    //private static final String wzor = "(0?[1-9]|[12][0-9]|3[01])\\.(0?[1-9]|1[012])\\.((19|20)\\d\\d)";

    private static final String wzor = "((19|20)\\d\\d)\\.(0?[1-9]|1[012])\\.(0?[1-9]|[12][0-9]|3[01])";

    private static final String DEBUG_TAG = "ErrorDodaj:";
    Pattern wzorzecDaty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj);
        em = new ZadaniaSQLITE(this);
        em.open();

        poleNazwaZadania = findViewById(R.id.poleNazwaZadania);
        poleTermin = findViewById(R.id.poleTermin);
        poleDlugoscWDniach = findViewById(R.id.poleDlugoscWDniach);
        suwakPilnoscZadania = findViewById(R.id.suwakPilnoscZadania);
        suwakWaznoscZadania = findViewById(R.id.suwakWaznoscZadania);


        wzorzecDaty = Pattern.compile(wzor);
        poleTermin.setText(Dane.getDomyslna_data());
}




    public boolean formatDatyJestDobry(String text) {
        Matcher matcher = wzorzecDaty.matcher(text);
        if(matcher.matches()){

            matcher.reset();

            if(matcher.find()){

                String day = matcher.group(3);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(1));

                if (day.equals("31") &&
                        (month.equals("4") || month .equals("6") || month.equals("9") ||
                                month.equals("11") || month.equals("04") || month .equals("06") ||
                                month.equals("09"))) {
                    return false; // only 1,3,5,7,8,10,12 has 31 days
                } else if (month.equals("2") || month.equals("02")) {
                    //leap year
                    if(year % 4==0){
                        if(day.equals("30") || day.equals("31")){
                            return false;
                        }else{
                            return true;
                        }
                    }else{
                        if(day.equals("29")||day.equals("30")||day.equals("31")){
                            return false;
                        }else{
                            return true;
                        }
                    }
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }






    public void zbudujIwyswietlOknaDialogowe(String tytul,String tresc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Dodaj.this);
        builder.setMessage(tresc).setTitle(tytul);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void klikDodaj(View view) {


        //Log.d(DEBUG_TAG, "poleNazwaZadania="+poleNazwaZadania);
        String nazwaZadania = poleNazwaZadania.getText().toString();
        String terminWykonania = poleTermin.getText().toString();
        if(!formatDatyJestDobry(terminWykonania)){
            zbudujIwyswietlOknaDialogowe("Blad","Podano nieprawidlowy format daty. Poprawny to yyyy.mm.dd\nalbo podana data nie istnieje");
            return;
        }

        int iloscPotrzebnychDni = Integer.parseInt(poleDlugoscWDniach.getText().toString());
        int waznosc = suwakPilnoscZadania.getProgress();
        int pilnosc = suwakWaznoscZadania.getProgress();
        Log.d(DEBUG_TAG, "W P =" + waznosc+" "+pilnosc);
       em.dodajRekordDoTabeliZadanie(nazwaZadania,terminWykonania,iloscPotrzebnychDni,waznosc,pilnosc,Dane.getIdZalogowanegoUzytkownika());


       Intent intencja = new Intent(Dodaj.this, MenuOrganizera.class);
       startActivity(intencja);
        finish();
    }



}
