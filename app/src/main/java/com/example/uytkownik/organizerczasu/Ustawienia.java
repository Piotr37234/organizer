package com.example.uytkownik.organizerczasu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

public class Ustawienia extends AppCompatActivity {

    int iloscZadanaNadzien;
    int waznoscPilnosc;
    int terminPilnosc;
    int duzoKrotkichMaloDlugich;



    private static final String DEBUG_TAG = "ErrorUstawienia:";
    public ZadaniaSQLITE em;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ustawienia);
        em = new ZadaniaSQLITE(this);
        em.open();
    }

    void odczytajWartosciZSuwakow() {
        SeekBar suwakIloscZadanaNadzien = findViewById(R.id.iloscZadanNaDzien);
        SeekBar suwakWaznoscPilnosc = findViewById(R.id.waznoscPilnosc);
        SeekBar suwakTerminPilnosc = findViewById(R.id.terminPilnosc);
        SeekBar suwakDuzoKrotkichMaloDlugich = findViewById(R.id.duzoKrotkichMaloDlugich);

        iloscZadanaNadzien = suwakIloscZadanaNadzien.getProgress();
        waznoscPilnosc = suwakWaznoscPilnosc.getProgress();
        terminPilnosc = suwakTerminPilnosc.getProgress();
        duzoKrotkichMaloDlugich = suwakDuzoKrotkichMaloDlugich.getProgress();
    }


    void wyslijZawartoscUstawienDoDanych() {
        Dane.setDuzoKrotkichMaloDlugich(duzoKrotkichMaloDlugich);
        Dane.setIloscZadanaNadzien(iloscZadanaNadzien);
        Dane.setTerminPilnosc(terminPilnosc);
        Dane.setIloscZadanaNadzien(iloscZadanaNadzien);
        Dane.setWaznoscPilnosc(waznoscPilnosc);
    }


   public void klikZatwierdz(View view) {



        odczytajWartosciZSuwakow();
        wyslijZawartoscUstawienDoDanych();
       Log.d(DEBUG_TAG, "WARTOSCI"+iloscZadanaNadzien + " " + waznoscPilnosc + " " + terminPilnosc + " " + duzoKrotkichMaloDlugich);
       em.usunRekordZTabeliUzytkownik(Dane.getIdZalogowanegoUzytkownika());
       em.dodajRekordDoTabeliUzytkownik(Dane.getIdZalogowanegoUzytkownika(), Dane.getImieZalogowany(),iloscZadanaNadzien,waznoscPilnosc,terminPilnosc,duzoKrotkichMaloDlugich);

       Log.d(DEBUG_TAG, "WARTOSCI DANE"+Dane.getIloscZadanaNadzien() + " " + Dane.getWaznoscPilnosc() + " " + Dane.getTerminPilnosc() + " " + Dane.getDuzoKrotkichMaloDlugich());

       Intent intencja = new Intent(Ustawienia.this, MenuOrganizera.class);
       startActivity(intencja);
       finish();

    }


}
