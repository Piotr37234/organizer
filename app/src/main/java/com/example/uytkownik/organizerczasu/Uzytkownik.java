package com.example.uytkownik.organizerczasu;

/**
 * Created by użytkownik on 2018-06-29.
 */




public class Uzytkownik {

    int id;
    String imie;
    int iloscZadanaNadzien;
    int waznoscPilnosc;
    int terminPilnosc;
    int duzoKrotkichMaloDlugich;



    public Uzytkownik(int id, String imie, int iloscZadanaNadzien, int waznoscPilnosc, int terminPilnosc, int duzoKrotkichMaloDlugich) {
        this.id = id;
        this.imie = imie;
        this.iloscZadanaNadzien = iloscZadanaNadzien;
        this.waznoscPilnosc = waznoscPilnosc;
        this.terminPilnosc = terminPilnosc;
        this.duzoKrotkichMaloDlugich = duzoKrotkichMaloDlugich;
    }





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public int getTerminPilnosc() {
        return terminPilnosc;
    }

    public void setTerminPilnosc(int terminPilnosc) {
        this.terminPilnosc = terminPilnosc;
    }



    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getIloscZadanaNadzien() {
        return iloscZadanaNadzien;
    }

    public void setIloscZadanaNadzien(int iloscZadanaNadzien) {
        this.iloscZadanaNadzien = iloscZadanaNadzien;
    }

    public int getWaznoscPilnosc() {
        return waznoscPilnosc;
    }

    public void setWaznoscPilnosc(int waznoscPilnosc) {
        this.waznoscPilnosc = waznoscPilnosc;
    }


    public int getDuzoKrotkichMaloDlugich() {
        return duzoKrotkichMaloDlugich;
    }

    public void setDuzoKrotkichMaloDlugich(int duzoKrotkichMaloDlugich) {
        this.duzoKrotkichMaloDlugich = duzoKrotkichMaloDlugich;
    }









}
