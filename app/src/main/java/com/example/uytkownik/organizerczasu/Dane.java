package com.example.uytkownik.organizerczasu;

public class Dane {
    public static String IMIE_ZALOGOWANY = "";
    public static int ID_ZALOGOWANEGO_UZYTKOWNIKA=0;
    public static int iloscZadanaNadzien;
    public static int waznoscPilnosc;
    public static int terminPilnosc;
    public static int duzoKrotkichMaloDlugich;
    private static final String DEBUG_TAG = "ErrorDane:";

    public static String getDomyslna_data() {
        return domyslna_data;
    }

    public static void setDomyslna_data(String domyslna_data) {
        Dane.domyslna_data = domyslna_data;
    }

    private static String domyslna_data = "rrrr.mm.dd";


    public static String getImieZalogowany() {
        return IMIE_ZALOGOWANY;
    }

    public static void setImieZalogowany(String imieZalogowany) {
        IMIE_ZALOGOWANY = imieZalogowany;
    }

    public static int getIdZalogowanegoUzytkownika() {
        return ID_ZALOGOWANEGO_UZYTKOWNIKA;
    }

    public static void setIdZalogowanegoUzytkownika(int idZalogowanegoUzytkownika) {
        ID_ZALOGOWANEGO_UZYTKOWNIKA = idZalogowanegoUzytkownika;
    }








    public static int getIloscZadanaNadzien() {
        return iloscZadanaNadzien;
    }

    public static void setIloscZadanaNadzien(int iloscZadanaNadzien) {
        Dane.iloscZadanaNadzien = iloscZadanaNadzien;
    }

    public static int getWaznoscPilnosc() {
        return waznoscPilnosc;
    }

    public static void setWaznoscPilnosc(int waznoscPilnosc) {
        Dane.waznoscPilnosc = waznoscPilnosc;
    }

    public static int getTerminPilnosc() {
        return terminPilnosc;
    }

    public static void setTerminPilnosc(int terminPilnosc) {
        Dane.terminPilnosc = terminPilnosc;
    }

    public static int getDuzoKrotkichMaloDlugich() {
        return duzoKrotkichMaloDlugich;
    }

    public static void setDuzoKrotkichMaloDlugich(int duzoKrotkichMaloDlugich) {
        Dane.duzoKrotkichMaloDlugich = duzoKrotkichMaloDlugich;
    }

}
