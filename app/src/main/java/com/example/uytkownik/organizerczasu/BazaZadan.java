package com.example.uytkownik.organizerczasu;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class BazaZadan extends AppCompatActivity {

    int wybraneID = 0;
    public ZadaniaSQLITE em;
    int iloscRekordowWBazie;
    Cursor rekordy;
    TableLayout ukladTabeli;
    private static final String DEBUG_TAG = "ErrorBazaZadan:";

    public void dodajWiersz(TableLayout tabela, int id_wiersza, Zadanie zadanie) {
        TableRow row = new TableRow(this);
        row.setBackground(getDrawable(R.drawable.border_style));
        row.setId(id_wiersza);
        row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                klikZaznacz(v);
            }
        });
        TextView u1 = new TextView(this);
        u1.setText(zadanie.getNazwaZadania());
        u1.setTextSize(18);
        u1.setLayoutParams(new TableRow.LayoutParams(128, TableRow.LayoutParams.MATCH_PARENT));
        u1.setTextColor(Color.BLACK);
        u1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        TextView u2 = new TextView(this);
        u2.setText(zadanie.getTerminWykonania());
        u2.setLayoutParams(new TableRow.LayoutParams(58, TableRow.LayoutParams.MATCH_PARENT));
        u2.setTextColor(Color.BLACK);
        u2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        u2.setTextSize(18);
        TextView u3 = new TextView(this);
        u3.setText(""+zadanie.getIloscPotrzebnychDni());
        u3.setLayoutParams(new TableRow.LayoutParams(34, TableRow.LayoutParams.MATCH_PARENT));
        u3.setTextColor(Color.BLACK);
        u3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        u3.setTextSize(18);
        TextView u4 = new TextView(this);
        u4.setText(""+zadanie.getWaznosc());
        u4.setLayoutParams(new TableRow.LayoutParams(18, TableRow.LayoutParams.MATCH_PARENT));
        u4.setTextColor(Color.BLACK);
        u4.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        u4.setTextSize(18);
        TextView u5 = new TextView(this);
        u5.setText(""+zadanie.getPilnosc());
        u5.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        u5.setTextColor(Color.BLACK);
        u5.setTextSize(18);
        u5.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        row.addView(u1);
        row.addView(u2);
        row.addView(u3);
        row.addView(u4);
        row.addView(u5);
        tabela.addView(row);
    }


    void tworzWierszeBazy() {
        while (rekordy.moveToNext()) {
            int id = rekordy.getInt(0);
            String nazwaZadania = rekordy.getString(1);
            String terminWykonania = rekordy.getString(2);
            int iloscPotrzebnychDni = rekordy.getInt(3);
            int waznosc = rekordy.getInt(4);
            int pilnosc = rekordy.getInt(5);
            int id_uzykownika = rekordy.getInt(6);
            Zadanie nowe = new Zadanie(id, nazwaZadania, terminWykonania, iloscPotrzebnychDni, waznosc, pilnosc,id_uzykownika);
            dodajWiersz(ukladTabeli, id, nowe);
        }

    }



    public void odswierzListe(){
        Log.d(DEBUG_TAG, "smax=" + iloscRekordowWBazie);
        iloscRekordowWBazie = em.dajKursorZeWszystkimiZadaniamiZalogowanego().getCount();
        rekordy = em.dajKursorZeWszystkimiZadaniamiZalogowanego();
        tworzWierszeBazy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baza_zadan);
        ukladTabeli = findViewById(R.id.tabelka);

        em = new ZadaniaSQLITE(this);
        em.open();
        odswierzListe();


    }

    public void klikZaznacz(View view) {
        if (wybraneID != 0) {
            findViewById(wybraneID).setBackground(getDrawable(R.drawable.border_style));
        }
        //view.setBackgroundColor(Color.argb(21, 169, 211, 231));
        view.setBackground(getDrawable(R.drawable.selected_style));
        wybraneID = view.getId();
        //view.setBackground(R.color.colorPrimary);
    }



    public void klikUsun(View view) {
     if(wybraneID ==0){
         return;
     }
      em.usunRekordZTabeliZadanie(wybraneID);recreate();
     wybraneID = 0;
    }

    public void klikNoweZadanie(View view) {
        Log.d(DEBUG_TAG, "Wybrane ID=" + wybraneID);
        finish();
        Intent intencja = new Intent(BazaZadan.this, Dodaj.class);
        startActivity(intencja);

    }

    public void onBackPressed() {
        finish();
        Intent intencja = new Intent(BazaZadan.this, MenuOrganizera.class);
        startActivity(intencja);
    }


}
