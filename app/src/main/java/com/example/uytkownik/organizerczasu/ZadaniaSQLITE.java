package com.example.uytkownik.organizerczasu;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class ZadaniaSQLITE {

    private static final String DEBUG_TAG = "SQLITE";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "Bd2.db";
    private static final String NAZWA_TABELI = "zadanie2";
    private static final String NAZWA_TABELI2 = "UZYTKOWNIK2";
    private static ArrayList<Zadanie> listaZeWszystkimiZadaniami;
    private static ArrayList<Zadanie> listaZadanNaDzis;
    private static ArrayList<Uzytkownik> listaUzytkownikow;

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    private static final String CREATE_TABLE_ZADANIE =
            "CREATE TABLE "+NAZWA_TABELI+" (" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "nazwazadania TEXT," +
                    "terminwykonania TEXT," +
                    "iloscpotrzebnychdni INTEGER," +
                    "waznosc INTEGER," +
                    "pilnosc INTEGER," +
                    "id_uzytkownika INTEGER" +
                    ")";


    private static final String CREATE_TABLE_UZYTKOWNIK =
            "CREATE TABLE "+NAZWA_TABELI2+" (" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "imie TEXT," +
                    "iloscZadanaNadzien INTEGER," +
                    "waznoscPilnosc INTEGER," +
                    "iloscpotrzebnychdni INTEGER," +
                    "terminPilnosc INTEGER," +
                    "duzoKrotkichMaloDlugich INTEGER" +
                    ")";


    private static final String DROP_TABLE_ZADANIE = "DROP TABLE IF EXISTS "+NAZWA_TABELI+"";
    private static final String DROP_TABLE_UZYTKOWNIK = "DROP TABLE IF EXISTS "+NAZWA_TABELI2+"";
    private static final String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc","id_uzytkownika"};
    private static final String[] kolumny_tabeli_uzytkownicy = {"id", "imie", "iloscZadanaNadzien", "waznoscPilnosc", "iloscpotrzebnychdni", "terminPilnosc","duzoKrotkichMaloDlugich"};

    public ZadaniaSQLITE(Context context) {
        this.context = context;
        listaZeWszystkimiZadaniami = new ArrayList<>();
        listaZadanNaDzis = new ArrayList<>();
        listaUzytkownikow = new ArrayList<>();



    }

    public ZadaniaSQLITE open() {
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_ZADANIE);
            db.execSQL(CREATE_TABLE_UZYTKOWNIK);
            Log.d(DEBUG_TAG, "Stworzylem Tabele");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_TABLE_ZADANIE);
            db.execSQL(DROP_TABLE_UZYTKOWNIK);
            onCreate(db);

        }
    }

    public ArrayList<Uzytkownik> getListaUzytkownikowIAktualizuj() {
        Cursor cursor = db.query(NAZWA_TABELI2, kolumny_tabeli_uzytkownicy, "imie<>\"\"", null, null, null, null);
        wypelnijListeUzytkownikow(cursor, listaUzytkownikow);
        return listaUzytkownikow;
    }

    public long dodajRekordDoTabeliZadanie(String nazwaZadania, String terminWykonania, int iloscPotrzebnychDni, int waznosc, int pilnosc, int id_uzytkownika) {
        ContentValues newRecord = new ContentValues();
        newRecord.put("nazwazadania", nazwaZadania);
        newRecord.put("terminWykonania", terminWykonania);
        newRecord.put("iloscPotrzebnychDni", iloscPotrzebnychDni);
        newRecord.put("waznosc", waznosc);
        newRecord.put("pilnosc", pilnosc);
        newRecord.put("id_uzytkownika", id_uzytkownika);
        return db.insert(NAZWA_TABELI, null, newRecord);
    }

    public long dodajRekordDoTabeliUzytkownik(String imie, int iloscZadanaNadzien, int waznoscPilnosc,int terminPilnosc,int duzoKrotkichMaloDlugich) {
        ContentValues newRecord = new ContentValues();
        newRecord.put("imie", imie);
        newRecord.put("iloscZadanaNadzien", iloscZadanaNadzien);
        newRecord.put("waznoscPilnosc", waznoscPilnosc);
        newRecord.put("terminPilnosc", terminPilnosc);
        newRecord.put("duzoKrotkichMaloDlugich", duzoKrotkichMaloDlugich);
        return db.insert(NAZWA_TABELI2, null, newRecord);
    }


    public long dodajRekordDoTabeliUzytkownik(int id,String imie, int iloscZadanaNadzien, int waznoscPilnosc,int terminPilnosc,int duzoKrotkichMaloDlugich) {
        ContentValues newRecord = new ContentValues();
        newRecord.put("id",id);
        newRecord.put("imie", imie);
        newRecord.put("iloscZadanaNadzien", iloscZadanaNadzien);
        newRecord.put("waznoscPilnosc", waznoscPilnosc);
        newRecord.put("terminPilnosc", terminPilnosc);
        newRecord.put("duzoKrotkichMaloDlugich", duzoKrotkichMaloDlugich);
        return db.insert(NAZWA_TABELI2, null, newRecord);
    }




    public boolean usunRekordZTabeliZadanie(long id) {
        String warunek = "id=" + id;
        return db.delete(NAZWA_TABELI, warunek, null) > 0;
    }

    public boolean usunRekordZTabeliUzytkownik(long id) {
        String warunek = "id=" + id;
        return db.delete(NAZWA_TABELI2, warunek, null) > 0;
    }

    public void usunTabele() {
        db.execSQL(DROP_TABLE_UZYTKOWNIK);
        db.execSQL(DROP_TABLE_ZADANIE);
    }

    public Cursor dajKursorZeWszystkimiZadaniamiZalogowanego() {
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        return db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, null);
    }

    public ArrayList<Zadanie> getListaZeWszystkimiZadaniamiIAktualizuj() {
       // Log.d(DEBUG_TAG, "nowe=" + nowe);
        Cursor cursor = dajKursorZeWszystkimiZadaniamiZalogowanego();
        Log.d(DEBUG_TAG, "cursor=" + cursor);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String nazwaZadania = cursor.getString(1);
            String terminWykonania = cursor.getString(2);
            int iloscPotrzebnychDni = cursor.getInt(3);
            int waznosc = cursor.getInt(4);
            int pilnosc = cursor.getInt(5);
            int id_uzytkownika = cursor.getInt(6);
            Zadanie nowe = new Zadanie(id, nazwaZadania, terminWykonania, iloscPotrzebnychDni, waznosc, pilnosc, id_uzytkownika);
            Log.d(DEBUG_TAG, "nowe=" + nowe);
            listaZeWszystkimiZadaniami.add(nowe);
        }
        return listaZeWszystkimiZadaniami;
    }

    public ArrayList<Zadanie> getListaZadanNaDzis() {
        return listaZadanNaDzis;
    }

    public ArrayList getListaZeWszystkimiZadaniami() {
        return listaZeWszystkimiZadaniami;
    }

    public Zadanie getZadanie(int id) {
//        String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String warunek = "id=" + id;
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, null);
        Zadanie task = null;
        if (cursor != null && cursor.moveToFirst()) {
            String nazwaZadania = cursor.getString(1);
            String terminWykonania = cursor.getString(2);
            int iloscPotrzebnychDni = cursor.getInt(3);
            int waznosc = cursor.getInt(4);
            int pilnosc = cursor.getInt(5);
            int id_uzytkownika = cursor.getInt(6);
            task = new Zadanie(id, nazwaZadania, terminWykonania, iloscPotrzebnychDni, waznosc, pilnosc, id_uzytkownika);
        }
        return task;
    }

    public Uzytkownik getUzytkownik(int id) {
        //String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String warunek = "id=" + id;
        Cursor cursor = db.query(NAZWA_TABELI2, kolumny_tabeli_uzytkownicy, warunek, null, null, null, null);
        Uzytkownik uzytkownik = null;
        if (cursor != null && cursor.moveToFirst()) {
            String imie = cursor.getString(1);
            int iloscZadanaNadzien = cursor.getInt(2);
            int waznoscPilnosc = cursor.getInt(3);
            int terminPilnosc = cursor.getInt(4);
            int duzoKrotkichMaloDlugich = cursor.getInt(5);
            uzytkownik = new Uzytkownik(id, imie, iloscZadanaNadzien, waznoscPilnosc,terminPilnosc, duzoKrotkichMaloDlugich);
        }
        return uzytkownik;
    }

    public Zadanie getKursorZZadaniamiSpelniajacymiWarunek(String warunek) {
        //    String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, null);
        Zadanie task = null;
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(0);
            String nazwaZadania = cursor.getString(1);
            String terminWykonania = cursor.getString(2);
            int iloscPotrzebnychDni = cursor.getInt(3);
            int waznosc = cursor.getInt(4);
            int pilnosc = cursor.getInt(5);
            int id_uzytkownika = cursor.getInt(6);
            task = new Zadanie(id, nazwaZadania, terminWykonania, iloscPotrzebnychDni, waznosc, pilnosc, id_uzytkownika);
        }
        return task;
    }

    public void wypelnijListeZadan(Cursor cursor, ArrayList<Zadanie> listArray) {
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String nazwaZadania = cursor.getString(1);
            String terminWykonania = cursor.getString(2);
            int iloscPotrzebnychDni = cursor.getInt(3);
            int waznosc = cursor.getInt(4);
            int pilnosc = cursor.getInt(5);
            int id_uzytkownika = cursor.getInt(6);
            Zadanie nowe = new Zadanie(id, nazwaZadania, terminWykonania, iloscPotrzebnychDni, waznosc, pilnosc, id_uzytkownika);
            listArray.add(nowe);
        }
    }

    public void wypelnijListeUzytkownikow(Cursor cursor, ArrayList<Uzytkownik> listArray) {
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String imie = cursor.getString(1);
            int iloscZadanaNadzien = cursor.getInt(2);
            int waznoscPilnosc = cursor.getInt(3);
            int terminPilnosc = cursor.getInt(4);
            int duzoKrotkichMaloDlugich = cursor.getInt(5);
            Uzytkownik nowe = new Uzytkownik(id, imie, iloscZadanaNadzien, waznoscPilnosc,terminPilnosc, duzoKrotkichMaloDlugich);
            listArray.add(nowe);
        }
    }

    public ArrayList<Zadanie> getLista3ZadanZNajdluzszymTermineRealizacji() {
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        //   String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "terminwykonania DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }

    public ArrayList<Zadanie> getLista3ZadanktoreTrzebaWykonacNajwczesniej() {
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        //    String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "terminwykonania";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }

    public ArrayList<Zadanie> getLista3NajkrotszychZadan() {
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        ArrayList<Zadanie> tmp = new ArrayList<>();
        //      String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "iloscpotrzebnychdni";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }

    public ArrayList<Zadanie> getLista3NajdluzszychZadan() {
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        //    String[] kolumny_tabeli_zadania = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "iloscpotrzebnychdni DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }

    public ArrayList<Zadanie> getLista3NajwazniejszychZadan() {
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        //String[] columns = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "waznosc DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }


    public ArrayList<Zadanie> getListaZadanZdaty() {
        String data = Dane.getDomyslna_data();
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika() +" AND "+ "terminwykonania=\""+data+"\"";
        //String[] columns = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "waznosc DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }







    public ArrayList<Zadanie> getLista3ZadanNajpilniejszych() {
        ArrayList<Zadanie> tmp = new ArrayList<>();
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
       // String[] columns = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        String order = "pilnosc DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, limit);
        wypelnijListeZadan(cursor, tmp);
        return tmp;
    }

    public int getIloscZadan() {
        String warunek = "id_uzytkownika=" +Dane.getIdZalogowanegoUzytkownika();
        String kolumna_ilosc[] = {"count(id)"};
        // String[] columns = {"id", "nazwazadania", "terminwykonania", "iloscpotrzebnychdni", "waznosc", "pilnosc"};
        //String order = "pilnosc DESC";
        String limit = "3";
        Cursor cursor = db.query(NAZWA_TABELI, kolumna_ilosc, warunek, null, null, null, null, null);
        cursor.moveToFirst();
        int wynik = cursor.getInt(0);
        return wynik;
    }



    public ArrayList<Zadanie> getListaZadanNaDzisIAktualizuj() {
        int waznoscPilnosc = Dane.getWaznoscPilnosc();
        int terminPilnosc = Dane.getTerminPilnosc();
        int duzoKrotkichMaloDlugich = Dane.getDuzoKrotkichMaloDlugich();
        int iloscZadanNaDzien = Dane.getIloscZadanaNadzien();
        int id_zalogowanego = Dane.getIdZalogowanegoUzytkownika();
        String order = "";
        Cursor cursor;
        if (terminPilnosc <= 50 && waznoscPilnosc <= 50) {
            Log.d(DEBUG_TAG, "WARTOSCI1 " + terminPilnosc + " " + waznoscPilnosc);
            order += "terminwykonania DESC, pilnosc DESC, waznosc DESC, ";
        } else if (terminPilnosc > 50 && waznoscPilnosc <= 50) {
            Log.d(DEBUG_TAG, "WARTOSCI2 " + terminPilnosc + " " + waznoscPilnosc);
            order += "pilnosc DESC, terminwykonania DESC, waznosc DESC, ";
        } else if (terminPilnosc > 50 && waznoscPilnosc > 50) {
            Log.d(DEBUG_TAG, "WARTOSCI3 " + terminPilnosc + " " + waznoscPilnosc);
            order += "pilnosc DESC, terminwykonania DESC, waznosc DESC, ";
        } else if (terminPilnosc <= 50 && waznoscPilnosc < 50) {
            Log.d(DEBUG_TAG, "WARTOSCI4 " + terminPilnosc + " " + waznoscPilnosc);
            order += "terminwykonania DESC, pilnosc DESC, waznosc DESC, ";
        }
        if (duzoKrotkichMaloDlugich >= 50) {
            order += "iloscpotrzebnychdni";
        } else {
            order += "iloscpotrzebnychdni DESC";
        }
        String warunek = "id_uzytkownika=" +id_zalogowanego;
        cursor = db.query(NAZWA_TABELI, kolumny_tabeli_zadania, warunek, null, null, null, order, "" + iloscZadanNaDzien);
        wypelnijListeZadan(cursor, listaZadanNaDzis);
        //Log.d(DEBUG_TAG, "waznoscPilnosc=" + waznoscPilnosc);
       // Log.d(DEBUG_TAG, "terminPilnosc=" + terminPilnosc);
      //  Log.d(DEBUG_TAG, "duzoKrotkichMaloDlugich=" + duzoKrotkichMaloDlugich);
      //  Log.d(DEBUG_TAG, "iloscZadanNaDzien=" + iloscZadanNaDzien);
        Log.d(DEBUG_TAG, "lista=" + listaZadanNaDzis);
        return listaZadanNaDzis;
    }







}
