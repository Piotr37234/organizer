package com.example.uytkownik.organizerczasu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class MenuOrganizera extends AppCompatActivity {

    public ZadaniaSQLITE em;
    TableLayout ukladTabeli;
    private static final String DEBUG_TAG = "ErrorMenuOrganizera:";
    int wybraneId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_organizera);
        ukladTabeli = findViewById(R.id.Tabela);
        ((TextView) findViewById(R.id.powitalneInfo)).setText(Dane.getImieZalogowany());

        em = new ZadaniaSQLITE(this);
        em.open();
        aktualizujWierszeTabeli();

    }

    public void klikBazaDanych(View view) {
        Intent intencja = new Intent(MenuOrganizera.this, BazaZadan.class);
        startActivity(intencja);
    }

    public void klikNoweZadanie(View view) {
        Intent intencja = new Intent(MenuOrganizera.this, Dodaj.class);
        startActivity(intencja);
    }

    public void klikPodsumowanie(View view) {
        Intent intencja = new Intent(MenuOrganizera.this, Podsumowanie.class);
        startActivity(intencja);
    }

    public void klikKalendarz(View view) {
        Intent intencja = new Intent(MenuOrganizera.this, Kalendarz.class);
        startActivity(intencja);
    }

    public void klikUstawienia(View view) {
        wybraneId = 0;
        Intent intencja = new Intent(MenuOrganizera.this, Ustawienia.class);
        startActivity(intencja);
    }

    public void klikZrobione(View view) {
        if (wybraneId == 0) {
            return;
        }
        em.usunRekordZTabeliZadanie(wybraneId);
        recreate();
        wybraneId = 0;
    }



    public void zbudujIwyswietlOknaDialogowe(String tytul,String tresc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuOrganizera.this);
        builder.setMessage(tresc).setTitle(tytul);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public void kilkdodajZadanieExtra(View view){
        int iloscZadan = em.getIloscZadan();
        int ustawionaIloscZadan = Dane.getIloscZadanaNadzien();
        int nowaUstawionaIloscZadan;
        if(ustawionaIloscZadan>=iloscZadan || iloscZadan<=0){
            zbudujIwyswietlOknaDialogowe("Blad","Nie ma wiecej zadan");
            return;
        }
        if (iloscZadan < ustawionaIloscZadan) {
            nowaUstawionaIloscZadan = ustawionaIloscZadan + 1;
        } else {
            nowaUstawionaIloscZadan = iloscZadan+1;
        }
        Dane.setIloscZadanaNadzien(nowaUstawionaIloscZadan);
        recreate();


    }

    public void klikOdloz(View view) {
        int iloscZadan = em.getIloscZadan();
        int ustawionaIloscZadan = Dane.getIloscZadanaNadzien();
        int nowaUstawionaIloscZadan;
        if(iloscZadan<=0 || ustawionaIloscZadan<=0){
            zbudujIwyswietlOknaDialogowe("Blad","Nie ma zadan");
            return;
        }
        if (iloscZadan >= ustawionaIloscZadan) {
            nowaUstawionaIloscZadan = ustawionaIloscZadan - 1;
        } else {
            nowaUstawionaIloscZadan = iloscZadan-1;
        }
        Dane.setIloscZadanaNadzien(nowaUstawionaIloscZadan);
        recreate();
    }


    public void dodajWiersz(TableLayout tabela, Zadanie zadanie) {
        TableRow row = new TableRow(this);
        row.setBackground(getDrawable(R.drawable.border_style));
        row.setId(zadanie.getId());
        row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                klikZaznaczSzczegoly(v);
            }
        });
        TextView u1 = new TextView(this);
        u1.setText(zadanie.getNazwaZadania());
        u1.setTextSize(16);
        u1.setLayoutParams(new TableRow.LayoutParams(128, TableRow.LayoutParams.MATCH_PARENT));
        u1.setTextColor(Color.BLACK);
        u1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        row.addView(u1);
        tabela.addView(row);
    }

    public void aktualizujWierszeTabeli() {

        for (Zadanie zadanie : em.getListaZadanNaDzisIAktualizuj()) {
            dodajWiersz(ukladTabeli, zadanie);
        }
    }

    public Zadanie dajZadanieZlistyZadanNaDzis(int id) {
        for (Zadanie zadanie : em.getListaZadanNaDzis()) {
            //
            Log.d(DEBUG_TAG, "KOLEJNE ID id=" + zadanie.id);
            if (zadanie.getId() == id) {
                return zadanie;
            }
        }
        return null;
    }

    public void klikZaznaczSzczegoly(View view) {
        if (wybraneId != 0) {
            findViewById(wybraneId).setBackground(getDrawable(R.drawable.border_style));
        }
        view.setBackgroundColor(Color.argb(21, 169, 211, 231));
        view.setBackground(getDrawable(R.drawable.selected_style));
        wybraneId = view.getId();
        Log.d(DEBUG_TAG, "Wybrane ID=" + wybraneId);


        Zadanie zadanie = dajZadanieZlistyZadanNaDzis(wybraneId);

        Log.d(DEBUG_TAG, "zadanie=" + zadanie);
        Log.d(DEBUG_TAG, "lista=" + em.getListaZadanNaDzisIAktualizuj().toString());
        String tekst = "Termin wykonania:\n" + zadanie.getTerminWykonania() + "\nSzacowany czas wykonania:\n" + zadanie.getIloscPotrzebnychDni() + " dni";
        ((TextView) findViewById(R.id.okienkoInfo)).setText(tekst);
    }

    public void onBackPressed() {
        finish();
        Intent intencja = new Intent(MenuOrganizera.this, MainActivity.class);
        startActivity(intencja);
    }
}
