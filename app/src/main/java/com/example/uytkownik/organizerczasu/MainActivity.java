package com.example.uytkownik.organizerczasu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int wybraneID = 0;
    public ZadaniaSQLITE em;
    TableLayout ukladTabeli;
    private static final String DEBUG_TAG = "ErrorMenu:";
    EditText poleImie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ukladTabeli = findViewById(R.id.tabelaUzytkownikow);
        poleImie = findViewById(R.id.poleImieNowyUzytkownik);

        em = new ZadaniaSQLITE(this);
        em.open();
        //ukladTabeli.removeAllViews();
        aktualizujWierszeTabeli();

    }

    public void zbudujIwyswietlOknaDialogowe(String tytul, String tresc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(tresc).setTitle(tytul);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void klikWczytaj(View view) {
        if (wybraneID == 0) {
            zbudujIwyswietlOknaDialogowe("Nie wybrano uzytkownika", "Należy najpierw wybrac profil uzytkownika\n");
            return;
        }

        ustawAktualnieZalogowanego(em.getUzytkownik(wybraneID));
        Log.d(DEBUG_TAG, "Uzytkownik wybr:"+em.getUzytkownik(wybraneID).getImie());
        Log.d(DEBUG_TAG, "Uzytkownik zalogowany:"+Dane.getImieZalogowany());
        finish();
        Intent intencja = new Intent(MainActivity.this, MenuOrganizera.class);
        startActivity(intencja);

    }

    public void usunUzytkownika(View view) {
        if (wybraneID == 0) {
            return;
        }
        em.usunRekordZTabeliUzytkownik(wybraneID);
        recreate();
        wybraneID = 0;
    }



    public void ustawAktualnieZalogowanego(Uzytkownik uzytkownik) {
        Dane.setIdZalogowanegoUzytkownika(uzytkownik.getId());
        Dane.setImieZalogowany(uzytkownik.getImie());
        Dane.setIloscZadanaNadzien(uzytkownik.getIloscZadanaNadzien());
        Dane.setTerminPilnosc(uzytkownik.getTerminPilnosc());
        Dane.setDuzoKrotkichMaloDlugich(uzytkownik.getDuzoKrotkichMaloDlugich());

        Log.d(DEBUG_TAG, "USTAWIANIE WAZNOSC PILNOSC :"+uzytkownik.getWaznoscPilnosc());
        Dane.setWaznoscPilnosc(uzytkownik.getWaznoscPilnosc());
    }


    public void dodajWiersz(TableLayout tabela, Uzytkownik uzytkownik) {
        TableRow row = new TableRow(this);
        row.setBackground(getDrawable(R.drawable.border_style));
        row.setId(uzytkownik.getId());
        row.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                klikZaznaczUser(v);
            }
        });
        TextView u1 = new TextView(this);
        u1.setText(uzytkownik.getImie());
        u1.setTextSize(24);
        u1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));
        u1.setTextColor(Color.BLACK);
        u1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        row.addView(u1);
        tabela.addView(row);
    }


    public void klikZaznaczUser(View view) {
        if (wybraneID != 0) {
            findViewById(wybraneID).setBackground(getDrawable(R.drawable.border_style3));
        }
        Log.d(DEBUG_TAG, "Wybrane ID=" + wybraneID);
        view.setBackground(getDrawable(R.drawable.selected_style2));
        wybraneID = view.getId();

        //TextView poleTekstowe = (TextView) ((TableRow) view).getChildAt(0);
        // Dane.IMIE_ZALOGOWANY = (String) poleTekstowe.getText();
        // Dane.ID_ZALOGOWANEGO_UZYTKOWNIKA = em.
    }

    public void klikNowyUzytkownik(View viev) {
        String imie = poleImie.getText().toString();
        em.dodajRekordDoTabeliUzytkownik(imie, 1, 50, 50, 50);
        recreate();
    }

    public void aktualizujWierszeTabeli() {
        for (Uzytkownik uzytkownik : em.getListaUzytkownikowIAktualizuj()) {
            Log.d(DEBUG_TAG, "Uzytkownik id:" + uzytkownik.getId() + "imie:" + uzytkownik.getImie());
            dodajWiersz(ukladTabeli, uzytkownik);
        }
    }

}
