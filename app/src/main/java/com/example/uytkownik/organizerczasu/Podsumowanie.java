package com.example.uytkownik.organizerczasu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;


public class Podsumowanie extends AppCompatActivity {

    public ZadaniaSQLITE em;
    private static final String DEBUG_TAG = "ErrorPodsumowanie:";
    TextView iloscZadanCzasowych;
    TextView iloscZadanWaznych;
    TextView iloscWszystkichZadan;
    TextView czasTrwaniaWszystkich;

    TextView najdluzszeZadania;
    TextView najkrotszeZadania;
    TextView zadaniaKtoreWykonacTrzebaNajwczensiej;
    TextView zadaniaZnajdluszymTerminem;
    TextView zadaniaNajwazniejsze;
    TextView zadaniaNajpilniejsze;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podsumowanie);
        em = new ZadaniaSQLITE(this);
        em.open();


       // iloscZadanCzasowych = findViewById(R.id.IloscZadanCzasowych);
      //  iloscZadanWaznych = findViewById(R.id.IloscZadanWaznych);
     //   iloscWszystkichZadan = findViewById(R.id.IloscWszystkichZadan);
     //   czasTrwaniaWszystkich = findViewById(R.id.CzasTrwaniaWszystkich);

        najdluzszeZadania = findViewById(R.id.najdluzszeZadania);
        najkrotszeZadania = findViewById(R.id.najkrotszeZadania);
        zadaniaKtoreWykonacTrzebaNajwczensiej = findViewById(R.id.ZadaniaKtoreWykonacTrzebaNajwczensiej);
        zadaniaZnajdluszymTerminem = findViewById(R.id.ZadaniaZnajdluszymTerminem);
        zadaniaNajwazniejsze = findViewById(R.id.ZadaniaNajwazniejsze);
        zadaniaNajpilniejsze = findViewById(R.id.ZadaniaNajpilniejsze);

        ustawWidoki();
    }

//kolejne zadanie algorytm

    void ustawWidoki() {
        najdluzszeZadania.setText(getTextZListyPionoweRozmieszczenie("Najdluzsze zadania:",em.getLista3NajdluzszychZadan()));
        najkrotszeZadania.setText(getTextZListyPionoweRozmieszczenie("Najkrótsze zadania:",em.getLista3NajkrotszychZadan()));
        zadaniaKtoreWykonacTrzebaNajwczensiej.setText(getTextZListyPionoweRozmieszczenieTerminy("Zadania które trzeba wykonać najwcześniej:",em.getLista3ZadanktoreTrzebaWykonacNajwczesniej()));
        zadaniaZnajdluszymTerminem.setText(getTextZListyPionoweRozmieszczenieTerminy("Zadania z najdluzszym terminem realizacji:",em.getLista3ZadanZNajdluzszymTermineRealizacji()));
        zadaniaNajwazniejsze.setText(getTextZListyPionoweRozmieszczenie("Zadania Najważniejsze:",em.getLista3NajwazniejszychZadan()));
        zadaniaNajpilniejsze.setText(getTextZListyPionoweRozmieszczenie("Zadania Najpilniejsze:",(em.getLista3ZadanNajpilniejszych())));

    }

    String getTextZListyPionoweRozmieszczenie(String naglowek,ArrayList<Zadanie> zadanieArrayList) {
        String text = naglowek + "\n\n";
        for (Zadanie zadanie : zadanieArrayList) {
            text += (zadanie.getNazwaZadania() + "\n");
        }
        return text;
    }

    String getTextZListyPoziomeRozmieszczenie(String naglowek,ArrayList<Zadanie> zadanieArrayList) {
        String text = naglowek + "\n\n";
        for (Zadanie zadanie : zadanieArrayList) {
            text += (zadanie.getNazwaZadania() + " ");
        }
        return text;
    }

    String getTextZListyPionoweRozmieszczenieTerminy(String naglowek,ArrayList<Zadanie> zadanieArrayList) {
        String text = naglowek + "\n\n";
        for (Zadanie zadanie : zadanieArrayList) {
            text += (zadanie.getNazwaZadania() + " ");
            text += (zadanie.getTerminWykonania() + " ");
            text += "\n";
        }
        return text;
    }

    public void onBackPressed() {
        finish();
        Intent intencja = new Intent(Podsumowanie.this, MenuOrganizera.class);
        startActivity(intencja);
    }


}
